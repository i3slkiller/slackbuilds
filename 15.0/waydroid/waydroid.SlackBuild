#!/bin/bash
# Copyright 2023  i3slkiller <i3sl.1.3.3.0@gmail.com> (PGP fingerprint: 7B69 A55C 4533 F5E2 44C2  4889 095A 0992 0ECC A5AF)
# All rights reserved.
#
#   Permission to use, copy, modify, and distribute this software for
#   any purpose with or without fee is hereby granted, provided that
#   the above copyright notice and this permission notice appear in all
#   copies.
#
#   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   SUCH DAMAGE.
# -----------------------------------------------------------------------------

PRGNAM=waydroid
VERSION=${VERSION:-1.4.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_pb}
PKGTYPE=${PKGTYPE:-txz}

if [ -z "$ARCH" ]; then
  case "$(uname -m)" in
    i?86) ARCH=i586 ;;
    *) ARCH=$(uname -m) ;;
  esac
fi

case "$ARCH" in
  i586) SLKCFLAGS="-O2 -march=i586 -mtune=i686"; SLKLDFLAGS=""; LIBDIRSUFFIX="" ;;
  x86_64) SLKCFLAGS="-O2 -fPIC"; SLKLDFLAGS="-L/usr/lib64"; LIBDIRSUFFIX="64" ;;
  *) SLKCFLAGS="-O2"; SLKLDFLAGS=""; LIBDIRSUFFIX="" ;;
esac

MAKEFLAGS=${MAKEFLAGS:=-j$(nproc)}

CWD="$(pwd)"
TMP=${TMP:-/tmp}
PKG="$TMP/pkg-$PRGNAM"
OUTPUT=${OUTPUT:-/tmp}

set -e

rm -rf "$PKG"
mkdir -p "$TMP" "$PKG" "$OUTPUT"
cd "$TMP"
rm -rf $PRGNAM-$VERSION
tar -xvf "$CWD"/$PRGNAM-$VERSION.tar.?z*
cd $PRGNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

make install USE_SYSTEMD=0 DESTDIR="$PKG"

mkdir -p "$PKG/usr/doc/$PRGNAM-$VERSION"
cp -a \
  CODE_OF_CONDUCT.md LICENSE README.md \
  "$PKG/usr/doc/$PRGNAM-$VERSION"
cat "$CWD/$PRGNAM.SlackBuild" > "$PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild"

mkdir -p "$PKG/install"
cat "$CWD/slack-desc" > "$PKG/install/slack-desc"
[ -f "$CWD/doinst.sh" ] && cat "$CWD/doinst.sh" > "$PKG/install/doinst.sh"

cd "$PKG"
/sbin/makepkg -l y -c n "$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
