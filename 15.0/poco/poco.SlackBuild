#!/bin/bash

# Slackware build script for poco

# Copyright 2014-2022  Dimitris Zlatanidis  Orestiada, Greece
# Copyright 2023  i3slkiller <i3sl.1.3.3.0@gmail.com> (PGP fingerprint: 7B69 A55C 4533 F5E2 44C2  4889 095A 0992 0ECC A5AF)
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# 29-DEC-2022: new maintainer, Edward W. Koenig (kingbeowulf)
# 18-OCT-2023: modified by i3slkiller (added options to omit MySQL, MongoDB and ODBC)

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=poco
VERSION=${VERSION:-1.12.4}
EDITION=all
BUILD=${BUILD:-2}
TAG=${TAG:-_SBo}
PKGTYPE=${PKGTYPE:-tgz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

# Make optionally detected components really optional as they are in 
# CMAKE build configuration upstream.
OMITCMPS=()

if [ ${POSTGRESQL:-no} = "no" ]; then
  OMITCMPS+=("Data/PostgreSQL")
fi

if [ ${MYSQL:-no} = "no" ]; then
  OMITCMPS+=("Data/MySQL")
fi

if [ ${MONGODB:-no} = "no" ]; then
  OMITCMPS+=("MongoDB")
fi

if [ ${ODBC:-no} = "no" ]; then
  OMITCMPS+=("Data/ODBC")
fi

if [ ${#OMITCMPS[*]} -gt 0 ]; then
  OMITARG="--omit="
  for (( i=0; i<${#OMITCMPS[*]}; i++ )); do
    OMITARG+=${OMITCMPS[i]}
    if [ $[${#OMITCMPS[*]} - 1] != $i ]; then
      OMITARG+=","
    fi
  done
else
  OMITARG=""
fi

TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION-$EDITION
tar xvf $CWD/$PRGNAM-$VERSION-$EDITION.tar.gz
cd $PRGNAM-$VERSION-$EDITION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

if [ "$ARCH" = "x86_64" ]; then
    patch -p1 < $CWD/poco_x64.patch
fi

# Slackware ships with MariaDB not MySQL, patch from Arch Linux
patch -p1 < $CWD/poco_mariadb.patch

./configure \
  --prefix=/usr \
  --no-tests \
  --no-samples \
  $OMITARG \
  --sqlite-thread-safe=2 \
  --shared

make
make install DESTDIR=$PKG

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

find $PKG -name perllocal.pod \
  -o -name ".packlist" \
  -o -name "*.bs" \
  | xargs rm -f

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a CHANGELOG CONTRIBUTORS NEWS LICENSE VERSION README $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

rm -f $PKG/usr/lib*/*.la

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
